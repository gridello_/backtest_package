from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='bcktst',
    version='0.1',
    packages=find_packages(),
    description='backtest trading ideas',
    long_description=readme(),
    zip_safe=False, install_requires=['numpy', 'pandas', 'scipy']

)

