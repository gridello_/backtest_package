## DOCUMENTATION
Yet another backtest package on the internet. Made with pandas, certainly not for for speed but for ease in visualization and understanding.
Inspired by backtrader and zipline.

*under development*


## INSTALL

```buildoutcfg:install
git clone git@gitlab.com:gridello_/backtest_package.git
cd bcktst
pip install -e .
```

## TUTORIAL

Basic structure:
* A Bundle (basically a dataframe) with the raw data is created.
* Indicators (data calculated from the Bundle data) are appended to the Bundle. Examples: rolling correlation, MoM change...
* A Broker is created. The broker passes and records trades.
* A Trader is created. The trader runs the data, calculates market positions from Indicators in the Bundle. It then uses its Broker to pass orders to the market.

All data (original data, Indicators, trades...) is recorded in a pandas Dataframe and ready to be plotted.


## TREE-MAP

```buildoutcfg:treemap

├── bcktst
│   ├── broker.py
│   ├── bundle.py
│   ├── eval_metrics.py
│   ├── indicator.py
│   ├── indicators.py
│   ├── __init__.py
│   ├── order.py
│   ├── periods.py
│   ├── portfolio_builder.py
│   ├── portfolio_builders.py
│   ├── trader.py
│   └── utils.py
├── example
│   ├── bcktst part 1 - strategy creation.ipynb
│   ├── bcktst part 2 - using bcktst.ipynb
│   └── data
│       ├── eia.csv
│       ├── results.csv
│       ├── trading_signal.csv
│       └── wti_ra.csv
├── README.md
└── setup.py



```
