"""
Collection of Indicator children.

"""

from .indicator import Indicator
from datetime import timedelta
from numpy import isnan


class ResampleMean(Indicator):
    """
    Weekly average of given variable
    """
    def __init__(self, column_name, resample='W-WED'):
        super().__init__(name='{}ResampleMean'.format(column_name))
        self.resample = resample
        self.column_name = column_name

    def next(self, data_df_subset, *args, **kwargs):
        """get resample.mean() of the given series
         given the output of resample.mean(), we have to cut the values past the last WED for example
         we only return the last value of the df"""

        resampled = data_df_subset[self.column_name].resample(self.resample).mean()
        try:  # expl: first date  in the data is Mon, resample() pushes mean() to a Wed in the future, don't want that
            return resampled.loc[:data_df_subset.index[-1]].iloc[-1]
        except IndexError:
            return resampled.iloc[-1]


class RollingCorrelation(Indicator):
    """
    Rolling correlation between avg prices and storage for a given window (def=13) .
    Ex: RollingCorrelation(data, window=13)
    corr_w(S, P)
    """
    def __init__(self, window=13):
        super().__init__(name='RollCorr', minperiod=timedelta(weeks=window))
        self.window = window

    def next(self, data_df_subset, *args, **kwargs):
        only_wednesdays = data_df_subset[data_df_subset.index.weekday == 2]
        avg_prices = only_wednesdays['closeResampleMean']
        storage = only_wednesdays['storage']
        return avg_prices.rolling(self.window).corr(storage).iloc[-1]


class CorrIndicator(Indicator):
    """
    Indicator of correlation: 1 or -1 if the corr gets past a thresh (def=0.33) in the expected direction (
    corr_trade, def=-1)
    Ex: CorrIndicator(data, corr_thre, corr_trade)

    sign(corr_w(S, P)) if abs(corr_w(S, P))*corr_trade>thresh else 0
    """
    def __init__(self, expected_corr, corr_threshold=0.33):
        super().__init__(name='CorrInd')
        self.corr_threshold = corr_threshold
        self.expected_corr = expected_corr

    def next(self, data_df_subset, *args, **kwargs):
        corr = data_df_subset['RollCorr'].iloc[-1]
        if corr > self.corr_threshold and self.expected_corr == 1:
            return 1
        elif corr < -self.corr_threshold and self.expected_corr == -1:
            return -1
        return 0


class DeltaSign(Indicator):
    """
    Sign of the variation of a given variable over delta weeks
    Note that we care about weekly de/increases so far at trading day (specific day in the week)
    """

    def __init__(self, column_name, delta, trading_day=2):  # 0 is Monday,...
        super().__init__(name='{}Delta{}Sign'.format(column_name, delta), minperiod=timedelta(weeks=delta))
        self.trading_day = trading_day
        self.delta = delta
        self.column_name = column_name

    def next(self, data_df_subset, *args, **kwargs):
        trading_days_only = data_df_subset[data_df_subset.index.weekday == self.trading_day][self.column_name]
        try:  # Since we only trade on a specific day, startdate+mminperiod may not contain delta trading days
            return 1 if trading_days_only[-1] > trading_days_only[-self.delta-1] else -1
        except IndexError:
            return


class StorageDR(Indicator):
    """ Storage Decision Rules
    prediction of price variation from inv delta, corr and ExpCorr"""

    def __init__(self, delta):
        super().__init__(name='StorageDRdelta{}'.format(delta), minperiod=timedelta(weeks=delta))
        self.delta = delta

    def pre_next(self, data_df_subset, *args, **kwargs):
        """ When minperiod hasnt been reached, the decision is always 0"""
        return 0

    def next(self, data_df_subset, *args, **kwargs):
        delta_sign = data_df_subset.iloc[-1]['storageDelta{}Sign'.format(self.delta)]
        if not isnan(delta_sign):
            return data_df_subset.iloc[-1]['CorrInd'] * delta_sign
        else:
            return 0
