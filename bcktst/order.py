class Order:
    """
    Records basic components of a market order.
    """

    def __init__(self, price, size, order_type, timestamp=None):
        self.price = price
        self.size = size
        self.order_type = order_type  # 'buy' or 'sell'
        self.timestamp = timestamp

    def describe(self):
        print('{}: {} {} contracts at ${}'.format(self.timestamp, self.order_type, self.size, self.price))
