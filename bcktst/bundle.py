from pandas.core.indexes.datetimes import DatetimeIndex
import pandas as pd


class Bundle:
    """
    Essentially an aligned pd.DataFrame
    """

    def __init__(self, input_datas, start_date=None, end_date=None):
        """
        :param input_datas: list of pd.series and/or df
        """
        self.first_valid_index = start_date if start_date else max([x.first_valid_index() for x in input_datas])
        self.last_valid_index = end_date if end_date else min([x.last_valid_index() for x in input_datas])
        self.data = self.aggregate(input_datas).fillna(method='ffill')[self.first_valid_index:self.last_valid_index]

    @staticmethod
    def aggregate(input_datas):
        out = pd.DataFrame()
        for data in input_datas:
            assert type(data.index) == DatetimeIndex
            out = out.join(data, how='outer')
        return out

    def get_data(self):
        return self.data

    def add_indicator(self, indicator):
        """
        add indicator Series/DataFrame to the current self.data
        :param indicator: pd.Series or pd.DataFrame. must have the same index as self.data
        """
        assert self.data.index.equals(indicator.index)

        if isinstance(indicator, pd.Series):
            self.data[indicator.name] = indicator

        elif isinstance(indicator, pd.DataFrame):
            for series_name in indicator:
                self.data[series_name] = indicator[series_name]

    def add_column(self, column):
        """
        To add a column to the self.data. This avoids having to run an Indicator on the data. Can introduce hindsight.
        :param column: pd.Series to be added to self.data
        :return:
        """
        assert column.index.equals(self.get_data().index), 'Column to be added and bundle.data have different indices'
        self.data = pd.concat([self.data, column], axis=1)

    def delete_column(self, column_name):
        self.data = self.data.drop(column_name, axis=1)

    def expand_as_df(self, col_to_expand, col_names=None):
        """
        Tome columns of .data (such as weights from EWA, are a pd.Series of lists.
        For easier graphing, this function expands the list as a pd.df
        :param col_to_expand: name of the column to expand
        :param col_names: names to associate with the returned df
        :return:
        """
        out = pd.DataFrame(index=self.data.index, columns=[0, 1, 2])

        for index, item in self.data[col_to_expand].iteritems():
            for i in range(len(self.data[col_to_expand].iloc[0])):
                out.loc[index, i] = item[i]
        if col_names is not None:
            out.columns = col_names
        return out
