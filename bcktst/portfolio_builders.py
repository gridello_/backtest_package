"""
Collection of Indicators children
TODO: ExponentialWeightAlgorithm is one of the less agile class of the package to say the least. AGILIFY!

"""

from .indicator import Indicator
from pandas import DataFrame
from datetime import timedelta
import numpy as np


class ExponentialWeightAlgorithm(Indicator):

    def __init__(self, predictors_names, objective_name, gamma,
                 horizon=timedelta(weeks=2), trading_day=2, loss_fct=np.square):
        """
        Note that by construction, we cannot get the minperiod from arguments. We thus have to handle it in self.next()
        :param predictors_names:
        :param objective_name:
        :param gamma:
        :param loss_fct:
        """
        if len(predictors_names) <= 1:
            raise ValueError('EWA was called with only {} predictors'.format(len(predictors_names)))

        super().__init__(name='EWA_vote', minperiod=horizon)
        self.predictors_names = predictors_names
        self.objective_name = objective_name
        self.gamma = gamma
        self.horizon = horizon
        self.loss_fct = loss_fct
        self.trading_day = trading_day  # only update weights when we trade

        self.weights = np.ones(len(predictors_names)) / len(predictors_names)

    def run(self, data_bundle, *args, **kwargs):
        """
        difference with Indicator.run:
         -returns multiple series: predictions but also weights, which are only updated on trading days
        """
        out = DataFrame(index=data_bundle.data.index, columns=['experts_weights', self.name])
        for timeindex in data_bundle.data.index:
            if timeindex >= data_bundle.data.index[0] + self.minperiod and timeindex.weekday() == self.trading_day:
                out.loc[timeindex, self.name] = self.next(data_bundle.data.loc[:timeindex], *args, **kwargs)
            out.loc[timeindex, 'experts_weights'] = self.weights
        return out

    def next(self, data_df_subset, *args, **kwargs):
        self.update_weigts_ewa(data_df_subset)
        last_preds = self.get_last_preds(data_df_subset)
        return self.get_majority_vote(last_preds)

    def update_weigts_ewa(self, data_df_subset):
        """
        Updates self.weights according to EWA
        """
        true = data_df_subset.iloc[-1][self.objective_name]
        unweighted_preds_h_ago = [data_df_subset.loc[data_df_subset.index[-1] - self.horizon][p] for p in
                                  self.predictors_names]
        weights_modifiers = np.exp(np.multiply(-self.gamma, [self.loss_fct(p - true) for p in unweighted_preds_h_ago]))
        self.weights = np.multiply(self.weights, weights_modifiers)
        self.weights = [w / sum(self.weights) for w in self.weights]

    def get_last_preds(self, data_df_subset):
        """
        Get a list of the weighted prediction
        """
        unweighted_preds_now = [data_df_subset.iloc[-1][p] for p in self.predictors_names]
        return np.multiply(unweighted_preds_now, self.weights)

    @staticmethod
    def get_majority_vote(predictions):
        """
        :param predictions: list of floats, weighted predictions from experts
        :return: the majority vote
        """
        return np.sign(sum(predictions))
