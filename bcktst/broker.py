from .order import Order
from numpy import floor


class Broker:
    """
    used to take position on the market, record cash and market position.
    """
    def __init__(self,  initial_cash=10000, max_order_size=500, max_mkt_position=None,
                 commission=0, allow_leverage=False):

        # Parameters
        self.max_order_size = max_order_size
        assert max_mkt_position is None or max_mkt_position >= 0, 'max_mkt_position must be >= 0'
        self.max_mkt_position = max_mkt_position
        self.commission = commission  # flat fee per contract
        self.allow_leverage = allow_leverage

        # Other attributes
        self.mkt_position = 0
        self.cash = initial_cash
        self.cumulative_fees_paid = 0

    def buy(self, price, size=1):
        """
        Create and return an Order. Update Attributes
        """
        assert size >= 0
        order_size = min(floor(size), self.max_order_size)
        if not self.allow_leverage:
            order_size = min(order_size, self.max_buy_size_no_leverage(price=price))
        if self.max_mkt_position is not None:  # Can only reach max. Handles current_pos>max_pos
            order_size = min(order_size, max(0, self.max_mkt_position - self.mkt_position))
        self.mkt_position += order_size
        fees = order_size * self.commission
        self.cash -= price * order_size + fees
        self.cumulative_fees_paid += fees
        return Order(price, order_size, 'buy')

    def sell(self, price, size=1):
        """
        Create and return an Order. Update Attributes
        """
        assert size >= 0
        order_size = min(floor(size), self.max_order_size)
        if self.max_mkt_position is not None:  # Can only reach max. Handles max_pos<current_pos
            order_size = min(order_size, max(0, self.mkt_position + self.max_mkt_position))
        self.mkt_position -= order_size
        fees = order_size * self.commission
        self.cash += price * order_size - fees
        self.cumulative_fees_paid += fees
        return Order(price, order_size, 'sell')

    def order(self, price, size):
        """
        basic wrapper for buy/sell: if size<0 calls self.sell otherwise calls self.buy
        """
        if size > 0:
            return self.buy(price, size)
        else:
            return self.sell(price, -size)

    def close_order(self, price, order):
        """
        Close a specific order
        :param price:
        :param order: Order, which we wish to close
        """
        return self.order(price=price, size=order.size)

    def close_mkt_position(self, price):
        """
        close all open positions at that time. Equivalent to getting out of the market
        """
        return self.order(price=price, size=-self.mkt_position)

    def order_percent(self, price, percentage):
        """
         order a percentage of current portfolio value.
         expl: if portfolio value is $100 and percentage is 0.2, take a $20 position in the mkt.
         TODO: check for unallowed leverage
        """
        return self.order(price=price, size=percentage * self.get_portfolio_value(price) / price)

    def order_target_size(self, price, target):
        """
        Place an order to adjust a position to a target size.
        This is equivalent to placing an order for the difference between the target size and the current mkt position.
        """
        return self.order(price, target - self.mkt_position)

    def order_target_percent(self, price, target):
        """
        Place an order to adjust a position to a target percent of the current portfolio value.
        This is equivalent to placing an order for the difference between the target percent and the current percent.
        """
        if self.allow_leverage and abs(target) > 1:
            raise ValueError('Leverage not allowed, but broker tried to order {}% of portfolio val.'.format(target*100))
        current_pct = price * self.mkt_position / self.get_portfolio_value(price)
        return self.order(price, size=(target-current_pct)*self.get_portfolio_value(price) / price)

    def max_buy_size_no_leverage(self, price):
        """
        Get the maximum allowed order size if  leverage is not allowed.
        order_type:  str, 'buy' or 'sell'
        """
        if price <= 0:  # On spreads, price can be negative and break the condition below.
            return self.max_order_size
        # Condition: after-order cash >= 0    <=>    cash-size(price+comm)>=0
        return floor(self.cash / (self.commission + price))

    def get_position(self):
        return self.mkt_position

    def get_position_value(self, price):
        """
        How much our current position in the market is valued at a given price
        """
        return self.mkt_position * price

    def get_cash(self):
        return self.cash

    def get_portfolio_value(self, price):
        return self.mkt_position*price + self.cash
