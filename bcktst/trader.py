from pandas import DataFrame
from datetime import timedelta


class Trader:
    """
    Reads tentative orders from a Bundle, sends them through its Broker, logs all that comes out.
    """

    def __init__(self, broker, data_bundle, new_trade_col_name='EWA_vote',
                 trading_day=2, horizon=timedelta(weeks=2)):
        """

        :param broker: Broker
        :param data_bundle: Bundle. data_bundle.data must contain a "close" column and a "order_target" column
        :param new_trade_col_name: str, name of the column that contains new mkt positions to take
        :param trading_day: int, weekday of trading. 2 is Wed. we do not trade every day. can be changed if we do.
        :param horizon: timedelta, how long we hold our position for.
        """

        self.data = data_bundle.data
        self.broker = broker
        self.order_target = new_trade_col_name
        self.trading_day = trading_day
        self.horizon = horizon
        self.results = {}

        self.order = None  # keep track of orders

    def next(self, data_df_subset):
        """
        send trades through self.broker
        """
        price = data_df_subset.close.iloc[-1]
        trading_days_targets = data_df_subset[data_df_subset.index.weekday == self.trading_day][self.order_target]
        target = trading_days_targets.iloc[-int(self.horizon.days/7):].mean()
        return self.broker.order_target_percent(price=price, target=target)

    def run(self):
        trading_start_date = self.data.dropna().first_valid_index()
        self.pretrade_log(trading_start_date)

        for timeindex in self.data[trading_start_date:].index:
            if timeindex.weekday() == self.trading_day:
                self.order = self.next(self.data.loc[:timeindex])
            self.log(self.data.loc[:timeindex])
            self.order = None

        return self.data.join(DataFrame.from_dict(self.results, orient='index'))

    def pretrade_log(self, trading_start_date):
        """
        Called before we place our first order. Logs
        :param trading_start_date: timestamp, date at which we start trading
        :return:
        """
        for timeindex in self.data[:trading_start_date].index:
            price = self.data.close.loc[timeindex]
            self.results[timeindex] = {'portfolio_value': self.broker.get_portfolio_value(price=price),
                                       'cash': self.broker.cash,
                                       'mkt_position': self.broker.mkt_position}

    def log(self, data_df_subset):
        """log all variables"""
        price = data_df_subset.close.iloc[-1]
        self.results[data_df_subset.index[-1]] = {'portfolio_value': self.broker.get_portfolio_value(price=price),
                                                  'cash': self.broker.get_cash(),
                                                  'mkt_position': self.broker.get_position()}
        if self.order:
            self.results[data_df_subset.index[-1]].update({'order_size': self.order.size,
                                                           'order_type': self.order.order_type})

    @staticmethod
    def console_log(message, timestamp):
        print('{}: {}'.format(timestamp.day(), message))