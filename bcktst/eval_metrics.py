"""
TODO: Description
TODO: detailed description
TODO: roll the ratios and other
TODO: Risk free: to be uniformized
"""

from .periods import DAILY, WEEKLY, MONTHLY, YEARLY, APPROX_DAYS_PER_YEAR
from .utils import annualization_factor
from scipy import stats
import pandas as pd
import numpy as np


def cum_returns(return_series: pd.Series):
    """
    Returns the cumulative returns.

    :param return_series:
    :return: (pd.Series)
    """
    out = np.empty_like(return_series.values)
    np.cumprod(np.add(return_series.values, 1), axis=0, out=out)
    np.subtract(out, 1, out=out)
    return pd.Series(out, index=return_series.index)


def aggregate_returns(return_series, period):
    """
    WARNING: takes a daily return series as input

    :param return_series:
    :param period:
    :return:

    """
    def cumulate_returns(x):
        return cum_returns(x).iloc[-1]

    if period == WEEKLY:
        grouper = pd.Grouper(freq='W')
    elif period == MONTHLY:
        grouper = pd.Grouper(freq='M')
    elif period == YEARLY:
        grouper = pd.Grouper(freq='Y')
    else:
        raise ValueError(
            'period argument must be {}, {} or {}'.format(WEEKLY, MONTHLY, YEARLY)
        )
    return return_series.groupby(grouper).apply(cumulate_returns)


def annual_return(return_series: pd.Series, period=DAILY, annualization=None):
    """
    Returns the mean annual growth rate of returns. This is equivalent
    to the compound annual growth rate.

    :param return_series:
    :param period:
    :param annualization:
    :return:
    """
    ann_factor = annualization_factor(period, annualization)
    num_years = len(return_series) / ann_factor
    result = (return_series + 1).prod()
    return result**(1/num_years)-1


class ReturnAnalysis:

    def __init__(self,
                 return_series: pd.Series,):
        self.return_series = return_series

    @staticmethod
    def simple_returns(price_series: pd.Series):
        """
        Returns simple returns (price var) from a time series of prices.

        :param price_series: (pd.Series)
        :return: (pd.Series)
        """
        return price_series.pct_change().iloc[1:]

    def adjust_returns(self, adjustment_factor_series: pd.Series):
        """
        Returns the returns series adjusted by adjustment_factor.

        :param adjustment_factor_series: (pd.Series)
        :return: (pd.Series
        """
        return self.return_series - adjustment_factor_series

    def max_drawdown(self):
        """
        :return:

        """
        max_return = np.fmax.accumulate((self.return_series + 1).cumprod())-1
        cum_return = cum_returns(self.return_series)
        return (cum_return - max_return) / max_return

    def annual_return(self, period=DAILY, annualization=None):
        """
        Returns the mean annual growth rate of returns. This is equivalent
        to the compound annual growth rate.

        :param period:
        :param annualization:
        :return:

        """
        ann_factor = annualization_factor(period, annualization)
        num_years = len(self.return_series) / ann_factor
        result = (self.return_series + 1).prod()
        return result**(1/num_years)-1

    def annual_volatility(self, period=DAILY, annualization=None):
        """
        Returns the annual volatility of a strategy.

        :param period:
        :param annualization:
        :return:
        """
        ann_factor = annualization_factor(period, annualization)
        return self.return_series.std() * np.sqrt(ann_factor)

    def stability_returns(self):
        """
        Returns the R-squared of a linear fit to the cumulative log returns.

        The stability_returns computes an ordinary least sqaures linear fit, and returns
        the R-squared
        :return:

        Warning: returns below 100% yield negative
        """
        cum_log_returns = np.log(1 + self.return_series).cumsum()
        rhat = stats.linregress(np.arange(len(cum_log_returns)), cum_log_returns)[2]
        return rhat**2

    def value_at_risk(self, cutoff: float=.05):
        """
        Returns the Value at Risk (VaR) of a series of returns.

        :param cutoff:
        :return:
        """
        return np.percentile(self.return_series.values, cutoff*100)

    def conditional_value_at_risk(self, cutoff: float=.05):
        """
        Returns the Conditional Value at Risk Return (CVaR) of a series of returns.

        CVaR measures the average return of returns below the cutoff percentile
        of the returns distribution.

        :param cutoff:
        :return:
        """
        return self.return_series[self.return_series <= self.value_at_risk(cutoff=cutoff)].mean()


class RatioAnalysis:

    def __init__(self, return_series: pd.Series):
        self.return_series = return_series
        self.return_analysis = ReturnAnalysis(self.return_series)

    def calmar_ratio(self, period=DAILY, annualization=None):
        """
        Returns the Calmar ratio (drawdown ratio), of a strategy.
        :return:

        """

        max_dd = min(self.return_analysis.max_drawdown())
        annual_return = self.return_analysis.annual_return(period, annualization)

        if max_dd < 0:
            return annual_return/abs(max_dd)
        else:
            return np.nan

    def omega_ratio(self, risk_free: float=0, required_return: float=0,
                    annualization=APPROX_DAYS_PER_YEAR):
        """
        :param risk_free: feds fund rate
        :param required_return:
        :param annualization:
        :return:

        """
        required_return_an = (1+required_return)**(1/annualization)-1

        return_less_threshold = self.return_series - (risk_free + required_return_an)
        num = sum(return_less_threshold[return_less_threshold > 0])
        denom = -1 * sum(return_less_threshold[return_less_threshold < 0])
        return num/denom if denom > 0 else np.nan

    def sharpe_ratio(self, risk_free: float=0, period=DAILY, annualization=None):
        """

        :param risk_free:
        :param period:
        :param annualization:
        :return:

        """
        return_risk_adj = self.return_series - risk_free
        ann_factor = annualization_factor(period, annualization)
        return return_risk_adj.mean()/return_risk_adj.std()*np.sqrt(ann_factor)

    def sortino_ratio(self, required_return: float=0, period=DAILY, annualization=None):
        """

        :param required_return:
        :param period:
        :param annualization:
        :return:

        Note: different from 'empyrical' implementation due to difference in downside_risk()
        """
        ann_factor = annualization_factor(period, annualization)
        required_return_an = (1+required_return)**(1/ann_factor)-1
        adj_return = self.return_analysis.adjust_returns(required_return_an)

        return adj_return.mean() * ann_factor / self.downside_risk(required_return, period, annualization)

    def excess_sharpe(self, adjustment_factor_series: pd.Series):
        """

        :param adjustment_factor_series:
        :return:
        """
        active_return = self.return_analysis.adjust_returns(adjustment_factor_series)
        return active_return.mean() / active_return.std()

    def downside_risk(self, required_return: float=0, period=DAILY, annualization=None):
        """
        Returns the downside risk of a strategy.
        :param required_return:
        :param period:
        :param annualization:
        :return:

        Note: Different from 'empyrical' version since they do not annualize the required return
        """

        ann_factor = annualization_factor(period, annualization)
        required_return_an = (1+required_return)**(1/ann_factor)-1
        adj_return = self.return_analysis.adjust_returns(required_return_an)
        downside_diff = np.clip(adj_return, np.NINF, 0)

        return downside_diff.std() * np.sqrt(ann_factor)

    def tail_ratio(self, low_tail: float=0.05, up_tail: float=.95):
        return abs(np.percentile(self.return_series, up_tail*100) / np.percentile(self.return_series, low_tail*100))

    def capture_ratio(self, adjustment_factor_series: pd.Series, **kwargs):
        """
        Returns the capture ratio of a strategy.
        :param adjustment_factor_series:
        :param kwargs: period {DAILY, WEEKLY, MONTHLY, YEARLY}, annualization (int)
        :return:
        """
        return annual_return(self.return_series, **kwargs) / annual_return(adjustment_factor_series, **kwargs)

    def up_capture_ratio(self, adjustment_factor_series: pd.Series, **kwargs):
        """
        Returns the up-capture ratio of a strategy.
        :param adjustment_factor_series:
        :param kwargs: period {DAILY, WEEKLY, MONTHLY, YEARLY}, annualization (int)
        :return:
        """
        idx = adjustment_factor_series.index[adjustment_factor_series > 0]
        return annual_return(self.return_series.loc[idx], **kwargs) / annual_return(adjustment_factor_series.loc[idx],
                                                                                    **kwargs)

    def down_capture_ratio(self, adjustment_factor_series: pd.Series, **kwargs):
        """
        Returns the down-capture ratio of a strategy.
        :param adjustment_factor_series:
        :param kwargs: period {DAILY, WEEKLY, MONTHLY, YEARLY}, annualization (int)
        :return:
        """
        idx = adjustment_factor_series.index[adjustment_factor_series < 0]
        return annual_return(self.return_series.loc[idx], **kwargs) / annual_return(adjustment_factor_series.loc[idx],
                                                                                    **kwargs)


class BenchmarkAnalysis:

    def __init__(self,
                 return_series: pd.Series,
                 adjustment_factor_series: pd.Series):
        self.return_series = return_series
        self.adjustment_factor_series = adjustment_factor_series
        self.aligned_df = self.aligned_ret_fact()

    def aligned_ret_fact(self):
        return self.return_series.rename('Y').to_frame().join(self.adjustment_factor_series.rename('X').to_frame())

    def alpha_beta(self, period=DAILY, annualization=None):
        """

        :return:
        """
        ann_factor = annualization_factor(period, annualization)
        ab_reg = stats.linregress(y=self.aligned_df['Y'].values, x=self.aligned_df['X'].values)
        return {'alpha': np.mean(self.aligned_df['Y'].values - ab_reg[0]*self.aligned_df['X'].values)*ann_factor,
                'beta': ab_reg[0]}

    def up_alpha_beta(self, period=DAILY, annualization=None):
        """

        :return:
        """
        ann_factor = annualization_factor(period, annualization)
        aligned_up_df = self.aligned_ret_fact()
        aligned_up_df = aligned_up_df[aligned_up_df['X'] > 0]
        ab_up_reg = stats.linregress(y=aligned_up_df['Y'].values, x=aligned_up_df['X'].values)
        return {'alpha_up': np.mean(aligned_up_df['Y'].values - ab_up_reg[0]*aligned_up_df['X'].values)*ann_factor,
                'beta_up': ab_up_reg[0]}

    def down_alpha_beta(self, period=DAILY, annualization=None):
        """

        :return:
        """
        ann_factor = annualization_factor(period, annualization)
        aligned_down_df = self.aligned_ret_fact()
        aligned_down_df = aligned_down_df[aligned_down_df['X'] < 0]
        ab_down_reg = stats.linregress(y=aligned_down_df['Y'].values, x=aligned_down_df['X'].values)
        return {'alpha_down': np.mean(aligned_down_df['Y'].values - ab_down_reg[0]*aligned_down_df[
            'X'].values)*ann_factor, 'beta_down': ab_down_reg[0]}
