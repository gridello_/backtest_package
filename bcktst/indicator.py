"""
The Indicator class is used to make calculation on Bundles data and add to that Bundle
By design, prevents any hindsight.

"""


from datetime import timedelta
from pandas import Series


class Indicator:

    def __init__(self, name='unnamed', minperiod=timedelta(0)):
        self.minperiod = minperiod
        self.name = name

    def pre_next(self, data_df_subset, *args, **kwargs):
        """
        Called by self.run() at each time period BEFORE minperiod is reached. Can be used to fill inconvenient Nan
        """
        pass  # To be overwritten by children

    def next(self, data_df_subset, *args, **kwargs):
        """
        Called by self.run() at each time period AFTER minperiod is reached. Used to calculate the indicator data.
        """
        pass  # To be overwritten by children

    def run(self, data_bundle, *args, **kwargs):
        """
        Given a Bundle, self.run() actually does the indicator calculation and returns a Series.
        :param data_bundle: Bundle
        :return:
        """
        out = Series(index=data_bundle.data.index, name=self.name)
        for timeindex in data_bundle.data.index:
            if timeindex < data_bundle.data.index[0] + self.minperiod:
                out.loc[timeindex] = self.pre_next(data_bundle.data.loc[:timeindex], *args, **kwargs)
            else:
                out.loc[timeindex] = self.next(data_bundle.data.loc[:timeindex], *args, **kwargs)
        return out
