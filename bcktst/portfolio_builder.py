"""
The PortfolioBuilder class is a child of Indicator with a specific purpose: aggregate multiple
decision rules into one portfolio

"""

from .indicator import Indicator
from pandas import Series
import numpy as np


class PortfolioBuilder(Indicator):
    """
    TODO
    this class is useless as is and is only being used for construction purposes right now. might change.
    """
    """
    creates a portfolio from one/multiple decision rules and an horizon
    """

    def __init__(self, predictors_names, voting_rule='ewa', horizon=1, objective_name=None):
        """

        :param predictors_names: list of str, names the columns
        :param voting_rule:
        :param horizon:
        :param objective_name:
        """
        super().__init__(name='order_target')
        self.predictors_names = predictors_names
        self.objective_name = objective_name
        self.voting_rule = voting_rule
        self.horizon = horizon

    def next(self, data_df_subset, *args, **kwargs):
        vote = None
        if self.voting_rule=='ewa':
            vote = self.get_ewa(data_df_subset)
        else:
            raise ValueError('PortfolioBuilder is not yet implemented with {}'.format(self.voting_rule))

        return vote

    def run(self, data_bundle, *args, **kwargs):
        """
        basically Indicator.run but takes into account horizon before returning
        :param data_bundle:
        :param args:
        :param kwargs:
        :return:
        """
        out = Series(index=data_bundle.data.index, name=self.name)
        for timeindex in data_bundle.data.index:
            if timeindex >= data_bundle.data.index[0] + self.minperiod:
                out.loc[timeindex] = self.next(data_bundle.data.loc[:timeindex], *args, **kwargs)

        return out.fillna(0).rolling(self.horizon).mean()  # fillna(0) because portfolio is 0 when DR are NaN

    def get_ewa(self, data_df_subset):
        if not self.true:
            raise ValueError('ewa was called with no objective_name')
        if len(self.predictors_names)<=1:
            raise ValueError('ewa was called with only {} predictors'.format(len(self.predictors_names)))
        true = data_df_subset.iloc[-1].true
        preds_h_ago = [data_df_subset.loc[-self.horizon, s] for s in self.predictors_names]
        weights_modifiers = np.exp(np.multiply(-self.gamma, [self.loss(p - true) for p in preds_h_ago]))
        self.weights = np.multiply(self.weights, weights_modifiers)
        self.weights = [w / sum(self.weights) for w in self.weights]
        preds_now = [data_df_subset.iloc[-1][s] for s in args]

