"""
TODO
"""

# from backtest.periods import DAILY, WEEKLY, MONTHLY, YEARLY
from .periods import ANNUALIZATION_FACTORS
import pandas as pd
import numpy as np


def annualization_factor(period, annualization):
    if annualization is None:
        try :
            factor = ANNUALIZATION_FACTORS[period]
        except KeyError:
            raise ValueError(
                "Period cannot be '{}'. "
                "Can be '{}'.".format(
                    period, "', '".join(ANNUALIZATION_FACTORS.keys())
                )
            )
    else:
        factor = annualization
    return factor


def _roll_pandas(func: callable, window: int, *args, **kwargs):
    data = {}
    index_values = []
    for i in range(window, len(args[0]) + 1):
        rets = [s.iloc[i-window:i] for s in args]
        index_value = args[0].index[i - 1]
        index_values.append(index_value)
        data[index_value] = func(*rets, **kwargs)
    return pd.Series(data, index=type(args[0].index)(index_values))


def get_direction(series, periods=1, thresh=0):
    """

    Args:
        series: pd.Series (levels of storage)
        thresh: (num) threshold of absolute variation under which the series is considered stagnant
        periods: param with which we call .diff(periods=periods). Example: working with weekly data, if periods=4 then
                 we calculate the sign of change MoM

    Returns:
        pd.df with the series, -1,0 or 1 if change is neg, null or pos, and the cluster index
        A cluster is a group of consecutive weeks where storage built/drew

    """

    def get_sign(x, threshold=0):
        try:
            return 0 if np.abs(x) < threshold else np.sign(x)
        except ValueError:
            return

    sign = series.diff(periods).apply(lambda x: get_sign(x, thresh)).rename('sign')
    inflection = (sign != sign.shift()).shift(-1).fillna(False).rename('inflection')
    clusters = inflection.cumsum().rename('cluster')

    df = pd.concat([series, sign, inflection, clusters], axis=1)

    return df
